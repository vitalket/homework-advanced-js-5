class Card {
  constructor(post, user) {
    this.post = post;
    this.user = user;
    this.createCard();
  }

  createCard() {
     const article = document.createElement('article');
        article.classList.add('post');

        const { name, email } = this.user;

        article.innerHTML = `
          <p class="name"><span>${name}</span> <a href="mailto:${email}">${email}</a></p>
          <h3 class="title">${this.post.title}</h3>
          <p class="message">${this.post.body}</p>
          <button class="button-delete">Delete</button>
        `;

        container.appendChild(article);

        const deleteButton = article.querySelector('.button-delete');
        
        deleteButton.addEventListener('click', () => {
            this.deleteCard(article);
        });
  }

  deleteCard(article) {
    if (article) {
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
            method: 'DELETE'
        })
        .then(response => {
            if (response.status === 200) {
                article.remove();
            } else {
                console.error('Помилка видалення');
            }
        })
        .catch(err => {
            console.error(err);
        });
    }
}
}

const container = document.querySelector('.container');
const preloader = document.querySelector('.preloader-container');

preloader.style.display = 'block';

fetch("https://ajax.test-danit.com/api/json/posts")
    .then(res => res.json())
    .then(posts => {
        preloader.style.display = 'none';
        fetch("https://ajax.test-danit.com/api/json/users")
            .then(res => res.json())
            .then(users => {
                posts.forEach(post => {
                    const user = users.find(el => el.id === post.userId);
                    new Card(post, user);
                });
            })
            .catch(err => {
                preloader.style.display = 'none';
                console.error(err);
            });
    })
    .catch(err => {
        preloader.style.display = 'none';
        console.error(err);
    });